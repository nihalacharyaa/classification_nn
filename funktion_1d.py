# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 14:45:07 2020

@author: Ajay N R
"""

import matplotlib.pyplot as plt


def genData(nSamples, withNoise=True,classified=False,frac=0.5):
    import numpy as np
    np.random.seed(0)
    
    sigmoid = lambda x: 1/(1+np.exp(-x))
    
    X = np.arange(0, 1, 1./nSamples)
    
    a1 = 1
    a2 = 1.5
    Z1 = sigmoid(a1*X)
    
    
    Z2 = sigmoid(a2*X)
    

    if withNoise:
        mu, sigma = 0, 0.1
        
        noise = np.random.normal(mu,sigma,X.shape[0])
        Z1 += noise
        Z1_nonoise=Z1-noise
        
        noise = np.random.normal(mu,sigma,X.shape[0])
        Z2 += noise
        Z2_nonoise=Z2-noise
    
    Z1, Z2 = np.stack((Z1,np.zeros(Z1.size)),axis=-1), np.stack((Z2,np.ones(Z2.size)),axis=-1)
    
    if classified:
        return X, Z1, Z2   
        
    else:
        ix1 = np.random.choice(X.size, int(np.round(frac*X.size)), replace=False)
        trainData = np.hstack([X[ix1,np.newaxis],Z1[ix1,:]])
        ix2 = [z for z in range(X.size) if not z in ix1]
        trainData = np.vstack([trainData,np.hstack([X[ix2,np.newaxis],Z2[ix2,:]])])
        trainData = trainData[trainData[:,0].argsort()]        
        
        return trainData,Z1_nonoise,Z2_nonoise

def pltcolor(lst):
    cols=[]
    for l in lst:
        if l==0:
            cols.append('orange')
        else:
            cols.append('blue')
    return cols
       
if __name__ == "__main__":
    classified = False
    if classified:
        X, Z1, Z2 = genData(nSamples=200,withNoise=True,classified=classified)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(X,Z1[:,0])
        ax.scatter(X,Z2[:,0])
        
    else:
        trainData,Z1_nonoise,Z2_nonoise = genData(nSamples=200,frac=0.8)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cols = pltcolor(trainData[:,-1])
        ax.scatter(trainData[:,0],trainData[:,1],c=cols,s=50)
        ax.plot(trainData[:,0],Z1_nonoise,c='orange',linewidth='3')
        ax.plot(trainData[:,0],Z2_nonoise,c='blue',linewidth='3')
        
        plt.show()
        